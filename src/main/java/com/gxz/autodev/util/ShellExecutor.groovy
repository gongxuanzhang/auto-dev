package com.gxz.autodev.util

class ShellExecutor {

    String executeShell(String shell) {
        return shell.execute().text
    }

    String executeShell(String shell, File file) {
        return shell.execute(null, file).text
    }


}

