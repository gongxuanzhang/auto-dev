package com.gxz.autodev.util;

import java.io.File;

/**
 * @author gxz gongxuanzhang@foxmail.com
 **/
public class GroovyUtils {

    private static final ShellExecutor SHELL_EXECUTOR = new ShellExecutor();


    public static String shell(String shell) {
        return SHELL_EXECUTOR.executeShell(shell);
    }

    public static String shell(String shell, File file) {
        return SHELL_EXECUTOR.executeShell(shell,file);
    }








}
